import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule} from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';

import { AuthenticationService } from './services/authenticate/authenticate.service';
import { UserService } from './services/user/user.service';
import { ClientService } from './services/client/client.service';

import { LoginComponent } from './components/login/login.component';
import { SubscribeComponent } from './components/subscribe/subscribe.component';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { HeaderComponent } from './components/header/header.component';
import { DashBoardComponent } from './components/dash-board/dash-board.component';
import { DetailClientComponent } from './components/detail-client/detail-client.component';
import { MyClientsComponent } from './components/my-clients/my-clients.component';
import { StatsComponent } from './components/stats/stats.component';
import { AuthGuard } from './services/authenticate/auth-guard.service';

const appRoutes: Routes = [
  { path: '',               component: HomeComponent },
  { path: 'header',         component: HeaderComponent, canActivate: [AuthGuard] },
  { path: 'login',          component: LoginComponent },
  { path: 'subscribe',      component: SubscribeComponent },
  { path: 'dashboard',      component: DashBoardComponent, canActivate: [AuthGuard]  },
  { path: 'detail',         component: DetailClientComponent, canActivate: [AuthGuard]  },
  { path: 'clients',        component: MyClientsComponent, canActivate: [AuthGuard]  },
  { path: 'stats',          component: StatsComponent, canActivate: [AuthGuard]  },
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    LoginComponent,
    SubscribeComponent,
    DashBoardComponent,
    DetailClientComponent,
    MyClientsComponent,
    StatsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    )
  ],
  providers:  [
                HttpClientModule,
                AuthenticationService,
                UserService,
                ClientService,
                AuthGuard
              ],
  bootstrap: [AppComponent]
})
export class AppModule { }
