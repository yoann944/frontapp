import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-subscribe',
  templateUrl: './subscribe.component.html',
  styleUrls: ['./subscribe.component.css']
})
export class SubscribeComponent implements OnInit {

  private subscribe:any = {}

  constructor(private userSrvice: UserService, private router: Router) { }

  ngOnInit() {
  }

  private subForm()
  {
    console.log(this.subscribe)
    this.userSrvice.subscribe(this.subscribe).subscribe((data) => {
      console.log(data);
      if(data['_id'])
      {
        console.log('sub ok ');
        this.router.navigate(['/'])
      }
      else
      {
        console.log('not sub ');
        alert('Erreur d\'inscription verifier les champs du formulaire')
      }

    })
  }
}
