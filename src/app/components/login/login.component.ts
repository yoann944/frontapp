import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../services/authenticate/authenticate.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public loginForm: any = {};

  constructor(private authService: AuthenticationService, private router: Router) { }

  ngOnInit() {
  }

  private logForm() {
    this.authService.login(this.loginForm).subscribe((data) => {
      this.router.navigate(['dashboard']);
    });
  }
}
