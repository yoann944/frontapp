import { Component, OnInit } from '@angular/core';
import { Register } from '../../class/register';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  model = new Register(1, 'wampach', 'Yoann', 'PMC', 'yoann@hotmail.fr', 768687967, '0750979493');
  
  submitted = false;

  onSubmit() { this.submitted = true; }

  // TODO: Remove this when we're done
  get diagnostic() { return JSON.stringify(this.model); }

}  
