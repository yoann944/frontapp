import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Chart } from 'chart.js';
import { ClientService } from '../../services/client/client.service';
import { UserService } from '../../services/user/user.service';

import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-dash-board',
  templateUrl: './dash-board.component.html',
  styleUrls: ['./dash-board.component.css']
})
export class DashBoardComponent implements OnInit {
  @ViewChild('canvas') canvas: ElementRef;
  @ViewChild('canvas2') canvas2: ElementRef;

  private chart: any;
  private clients:any;
  private datas: any = [12, 23, 22, 43, 8]
  private datas2: any = [89, 78, 44, 12, 1]
  private labels: any = ['x', 'y', 'z', 'e', 'p']
  private radar: any;
  public user: any;


  constructor(private clientService: ClientService, private userService: UserService, private router: Router) { }
  ngOnInit() {
    this.getUserByID('5bffc5f2d4aaea1bd8a817dd')
  }

  ngAfterViewInit() {
    this.displayChart()
  }

  private getUserByID(id:any)
  {
    this.userService.getUserByID(id).subscribe((data) => {
      this.user = data;
      console.log(this.user);
      
    })
  }


  private displayChart() {
    this.chart = new Chart(this.canvas.nativeElement.getContext('2d'), {
      type: 'line',
      data: {
        labels: this.labels,
        datasets: [
          {
            data: this.datas,
            borderColor: "#3cba9f",
            fill: false
          },
          {
            data: this.datas2,
            borderColor: "#ffcc00",
            fill: false
          },
        ]
      },
      options: {
        legend: {
          display: false
        },
        scales: {
          xAxes: [{
            display: true
          }],
          yAxes: [{
            display: true
          }],
        }
      }
    });

    var marksData = {
      labels: ["CCA", "CC", "TVA", "TTC", "HT", "BestSeller"],
      datasets: [{
        label: "Y",
        backgroundColor: "rgba(200,0,0,0.2)",
        data: [65, 75, 70, 80, 60, 80]
      }, {
        label: "X",
        backgroundColor: "rgba(0,0,200,0.2)",
        data: [54, 65, 60, 70, 70, 75]
      }]
    };
     

    this.radar = new Chart(this.canvas2.nativeElement.getContext('2d'), {
      type: 'radar',
      data: marksData,
      // options: options
    });
  }
}
