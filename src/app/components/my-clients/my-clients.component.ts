import { Component, OnInit } from '@angular/core';
import { ClientService } from '../../services/client/client.service';
import { UserService } from '../../services/user/user.service';
import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-my-clients',
  templateUrl: './my-clients.component.html',
  styleUrls: ['./my-clients.component.css']
})
export class MyClientsComponent implements OnInit {

  public clients: any;
  public newClient:any = {}
  public updateSection: boolean;
  public deleteSection: boolean;
  private currentUser: any;
  sells: any;

  constructor(private clientService: ClientService, private userService: UserService, private router: Router) { }

  ngOnInit() {
    this.getClients();
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.getAllSells();
    this.updateSection = false;
    this.deleteSection = false;
  }

  public getAllSells()
  {
    this.userService.getAllSellsByUser(this.currentUser.user._id).subscribe((sells) => {

      this.sells = sells;
      console.log('sells : ');
      console.log(this.sells);
      
      
    })
  }

  public addClient()
  {
    console.log(this.newClient);
    this.clientService.addClient(this.newClient).subscribe((res) => {
      if(res['_id'])
      {
        this.newClient = ""
        this.getClients();
      }      
    })
  }

  public updateView(id:string)
  {
    this.updateSection = true;
  }
  public deleteView(id:string)
  {
    this.deleteSection = true;
  }

  private getClients() {
    this.clientService.getClients().subscribe((data) => {
      console.log(data)
      this.clients = data;
    })
  }

  public selectedClient(id: any) {
    console.log(id)

    let navigationExtras: NavigationExtras = {
      queryParams: {
        "id": id
      }
    };
    this.router.navigate(['/detail'], navigationExtras)
  }
}
