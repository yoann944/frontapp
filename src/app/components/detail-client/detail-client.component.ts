import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { ClientService } from '../../services/client/client.service';
import { Router } from '@angular/router';
import { serializePath } from '@angular/router/src/url_tree';

@Component({
  selector: 'app-detail-client',
  templateUrl: './detail-client.component.html',
  styleUrls: ['./detail-client.component.css']
})
export class DetailClientComponent implements OnInit {

  public id: string;
  public client: any;
  public updateClient:any = {};
  private updateID: string;
  public updateFormView: boolean;

  constructor(private route: ActivatedRoute, private router: Router, private clientService: ClientService) {

  }

  ngOnInit() {
    this.getClient();
    this.updateFormView = false;
    
  }

  public updateView(id: string)
  {
    this.updateFormView = true;
    this.updateID = id;
    this.updateClient = this.client;
  }
  public cancelUpdateView()
  {
    this.updateFormView = false;
    this.updateID = "";
  }

  public deleteClient(id: string)
  {
    console.log('delte client : ' + id);
    this.clientService.deleteClientByID(id).then(() =>{
      
    })
    this.router.navigate(['/clients'])
  }

  public updateClientForm()
  {
    this.updateFormView = false;

    console.log('update : '+this.updateID);
    ///post 
    this.clientService.updateClientByID(this.updateID, this.updateClient).subscribe((res) => {
      console.log(res);
    })
  }


  private getClient() {
    this.route.queryParams.subscribe(params => {
      this.id = params["id"];
      console.log(this.id);
    });

    this.getClientByID(this.id)
  }

  private getClientByID(id: any) {
    this.clientService.getClientByID(id).subscribe((data) => {
      console.log(data);
      this.client = data;
    })
  }

}
