import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import * as JWT from 'jwt-decode';

@Injectable()
export class AuthenticationService {
  public token: any;
  public user: any;

  constructor(private http: HttpClient) { }

  public login(loginForm): any {
    return this.http
      .post('http://localhost:3000/authentication', loginForm)
      .map((response: any) => {
        console.log(response);

        const token: string = response.token;
        const responseUser: any = response.user;
        if (token !== undefined && responseUser !== undefined) {
          this.token = token;
          this.user = responseUser;
          localStorage.setItem('currentUser',
            JSON.stringify({ user: responseUser, token: token })
          );

          console.log(this.isAuthenticated());

          return this.user;
        } else {
          throw new Error('No user or token !');
        }
      }, err => {
        console.log(err);
        throw new Error(err);
      });

  }

  public logout() {
    localStorage.removeItem('currentUser');
  }

  public isAuthenticated(): boolean {
    const user: any = JSON.parse(localStorage.getItem('currentUser'));
    const jwtToken: string = user.token;
    const decoded: any = JWT(jwtToken);
    console.log(decoded);

    const current_time: number = new Date().getTime() / 1000;
    return  current_time <= decoded.exp;
    // return false;
  }

}
