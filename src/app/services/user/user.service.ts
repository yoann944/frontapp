import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class UserService {

  constructor(private http: HttpClient) { }

  public subscribe(subForm): Observable<any> {
    return this.http.post('http://localhost:3000/users', subForm);
  }

  public getUserByID(id: any): Observable<any> {
    return this.http.get(`http://localhost:3000/user/${id}`);
  }

  public getAllSellsByUser(id:string): Observable<any> {
    return this.http.get(`http://localhost:3000/user/${id}/sells`);
  }
}
