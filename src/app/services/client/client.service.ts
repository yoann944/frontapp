import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ClientService {

  constructor(private http: HttpClient) { }

  public addClient(client)
  {
    return this.http.post('http://localhost:3000/clients', client)
  }

  public getClients(): Observable<any> {

    return this.http.get('http://localhost:3000/clients')
  }

  public getClientByID(id: string) : Observable<any>
  {
    return this.http.get('http://localhost:3000/client/'+id)
  }

  public updateClientByID(id:string, updateClient)
  {
    return this.http.put('http://localhost:3000/client/'+id, updateClient)
  }

  public deleteClientByID(id:string)
  {
    return this.http.delete('http://localhost:3000/client/'+id).toPromise()
  }
}


