export class Register {

    constructor(

        public id: number,
        public nom: string,
        public prenom: string,
        public societe: string,
        public mail: string,
        public siret: number,
        public tel: string

    ) {}

}
